from equations_calculator import *


def get_modulo(i, mod, val):
    if i % mod == 0:
        return val
    else:
        return 0


def main():
    # File test
    draw_graph(solve_equation(data_importer.read_graph('test_data')), 'first_case.png')
    draw_graph(solve_equation([(i, (i + 1) % 16, (4 * i) % 12, get_modulo(i + 1, 14, 50)) for i in range(16)]),
               'second_case.png')
    draw_graph(solve_equation([(i, (i + 1) % 20, (7 * i + 3) % 17, get_modulo(i + 1, 17, 100)) for i in range(20)] + [
        (i, (i + 13) % 17, (3 * i + 123) % 25, 0) for i in range(10)]), 'third_case.png')


if __name__ == '__main__':
    main()
