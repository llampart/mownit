

def read_graph(file_name):
    data = []
    with open(file_name, 'r') as f:
        for line in f:
            tmp = line.split(",")
            data.append((int(tmp[0]),int(tmp[1]),float(tmp[2]),float(tmp[3])))

    return data