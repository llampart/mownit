import networkx as nx
import gauss_jordan
import gauss_jordan
import matplotlib.colors as colors
import matplotlib.cm as cmx
from numpy.linalg import lstsq
from numpy import array
from numpy import dot
import data_importer

"""
graph_edges -
"""

"""cokolwiek"""


class CircuitEdge:
    def __init__(self, counter, **kwargs):
        self.resistance = kwargs.get('resistance', float(0))
        self.voltage = kwargs.get('voltage', float(0))
        self.first_node = kwargs.get('first_node', None)
        self.second_node = kwargs.get('second_node', None)
        self.counter = counter
        self.set = False

    def to_directed_edge(self, current):
        if current > 0:
            return self.first_node, self.second_node, current
        else:
            return self.second_node, self.first_node, current

    def __str__(self):
        return str((self.first_node, self.second_node, self.resistance, self.voltage))


zero_edge = CircuitEdge(int())


def special_calculus(x, v_min, v_max, delta):
    return delta*(x - v_min) / (v_max - v_min)


def draw_graph(graph,name):
    try:
        import matplotlib.pyplot as plt
        plt.clf()
        pos = nx.layout.spring_layout(graph,iterations=20000)
        jet = cm = plt.get_cmap('jet')
        scalar_map = cmx.ScalarMappable(
            norm=colors.Normalize(vmin=0, vmax=max([abs(d['current']) for u, v, d in graph.edges_iter(data=True)])),
            cmap=jet)

        color_list = []
        edge_labels = dict(
            [((u, v,), 'i: ' + str(abs(d['current']))[:5] + ' v: ' + str(d['voltage'])[:5])
             for u, v, d in graph.edges(data=True)])
        node_labels = {n: n for n in graph.nodes()}
        for e in graph.edges_iter(data=True):
            color_list.append(scalar_map.to_rgba(abs(e[2]['current'])))

        x_max = max([abs(d['current']) for u, v, d in graph.edges_iter(data=True)])
        x_min = min([abs(d['current']) for u, v, d in graph.edges_iter(data=True)])
        edge_widths = [special_calculus(abs(e[2]['current']), x_min, x_max, 4.5) + 3.0 for e in graph.edges_iter(data=True)]
        nx.draw_networkx_nodes(graph, pos, node_color='k',node_size=6)
        nx.draw_networkx_labels(graph, pos, labels=node_labels, font_color='r')
        nx.draw_networkx_edges(graph, pos, edge_color=color_list, width=edge_widths)
        nx.draw_networkx_edge_labels(graph, pos, edge_labels=edge_labels, font_size=4)
        plt.savefig(name,dpi=200)
    except ImportError:
        pass


def solve_equation(input_data):
    G, solution_b, solution_matrix, tmp_dict = first_stage(input_data)
    print("input")
    print(array(solution_matrix))
    print(array(solution_b))
    normalised_matrix, normalised_b = normalise_equation(solution_matrix, solution_b, G.number_of_edges())

    b_solution = [row[0] for row in normalised_b]
    solution = gauss_jordan.gauss_jordan_full_pivot(array(normalised_matrix), array(b_solution))
    graph_tmp = nx.Graph()
    for pos, number in enumerate(solution):
        tmp_current = number
        graph_tmp.add_edge(tmp_dict[pos].first_node, tmp_dict[pos].second_node, current=tmp_current,
                           voltage=tmp_dict[pos].voltage)

    return graph_tmp


def first_stage(input_data):
    solution_matrix = []
    solution_b = []
    G = nx.Graph()
    global_counter = 0
    tmp_dict = {}
    for (u, v, resistance, voltage) in input_data:
        G.add_edge(u, v, attr_dict=None,
                   edge=CircuitEdge(counter=global_counter, resistance=resistance, voltage=voltage, first_node=u,
                                    second_node=v))
        global_counter += 1
    cycles = nx.cycle_basis(G)
    for cycle in cycles:
        local_dict = {}
        for (index, node) in enumerate(cycle):
            tmp_edge = get_edge(G, cycle, index, node)
            tmp_dict[tmp_edge.counter] = tmp_edge
            local_dict[tmp_edge.counter] = tmp_edge
        solution_matrix.append([local_dict.get(i, zero_edge).resistance for i in range(0, G.number_of_edges())])

        tmp = float(0)
        for edge in local_dict.values():
            tmp += edge.voltage
        solution_b.append([tmp])
    for node in G.nodes():
        tmp_dict_w = {G.get_edge_data(node, tmp_node, None)['edge'].counter: float(1) * get_sign(
            G.get_edge_data(node, tmp_node, None)['edge'], node) for tmp_node in G.neighbors(node)}
        solution_matrix.append([tmp_dict_w.get(i, float(0)) for i in range(0, G.number_of_edges())])
        solution_b.append([float(0)])
    return G, solution_b, solution_matrix, tmp_dict


def get_sign(edge, node):
    if edge.first_node == node:
        return -1
    else:
        return 1


def normalise_equation(left_matrix, right_matrix, dim):
    t_matrix = array([
                         [row[i] for row in left_matrix] for i in range(0, dim)
                         ])

    normalised_left = dot(t_matrix, array(left_matrix))

    normalised_right = dot(t_matrix, array(right_matrix))
    return normalised_left, normalised_right


def get_edge(G, cycle, index, node):
    next_node = cycle[(index + 1) % len(cycle)]
    return assure_order(G, next_node, node)


def with_numpy(input_data):
    G, solution_b, solution_matrix, tmp_dict = first_stage(input_data)
    print(array(solution_matrix), solution_b)
    A = lstsq(solution_matrix, solution_b)
    solution = A[0]
    graph_tmp = nx.Graph()
    for pos, number in enumerate(solution):
        tmp_current = number[0]
        graph_tmp.add_edge(tmp_dict[pos].first_node, tmp_dict[pos].second_node, current=tmp_current,
                           voltage=tmp_dict[pos].voltage, number=pos)
    return graph_tmp


def assure_order(G, next_node, node):
    """
    This function swaps the nodes of the edge to conform the order in the circuts for algorithm
    :param G:
    :param next_node:
    :param node:
    :return:
    """
    tmp_edge = G.get_edge_data(node, next_node)['edge']
    if not tmp_edge.set:
        tmp_edge.first_node, tmp_edge.second_node = node, next_node
    else:
        if tmp_edge.first_node != node:
            tmp_edge.resistance *= -1
    return tmp_edge


if __name__ == '__main__':
    """draw_graph(
        with_numpy([(1, 2, 2.5, 0), (2, 3, 0.8, 0), (3, 0, 0.3, 1.2), (0, 1, 1.5, 0), (4, 5, 0.9, 1), (5, 6, 0.23, 0),
                    (6, 4, 1.2, 0), (1, 3, 1.0, 0.0)]))"""
    draw_graph(solve_equation(data_importer.read_graph('test_data')))
    tmp = nx.Graph()
