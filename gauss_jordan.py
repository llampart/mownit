from numpy import array, copy


def gauss_jordan_full_pivot(a, b):
    n, m = a.shape
    if n != m:
        raise ValueError('Wrong dimensions\n')
    col_id = [i for i in range(n)]
    for k in range(n):
        pivot_row, pivot_column = get_pivot(a, k, n)

        if pivot_row != k:
            a[k, :], a[pivot_row, :] = copy(a[pivot_row, :]), copy(a[k, :])
            b[k], b[pivot_row] = b[pivot_row], b[k]

        if pivot_column != k:
            a[:, k], a[:, pivot_column] = copy(a[:, pivot_column]), copy(a[:, k])
            col_id[k], col_id[pivot_column] = col_id[pivot_column], col_id[k]

        pivot = a[k, k]
        a[k, k:] = a[k, k:] / pivot
        b[k] = b[k] / pivot

        backwards_substitution(a, b, k)

    return array(restore_order(b, col_id))


def restore_order(b, col_id):
    n = len(col_id)
    tab = [0 for i in range(n)]
    for i in range(n):
        tab[col_id[i]] = b[i]
    return tab


def backwards_substitution(a, b, k):
    n = b.size
    for row in range(n):
        if row != k:
            b[row] = b[row] - a[row, k] * b[k]
            a[row, k:] = a[row, k:] - a[row, k] * a[k, k:]


def get_pivot(a, k, n):
    _row = k
    column = k
    for row in range(k + 1, n):
        for col in range(k, n):
            if abs(a[row, col]) > abs(a[_row, column]):
                _row = row
                column = col
    return _row, column
